#include "RBTree.h"

enum Color
{
    RED,
    BLACK
};

RBNode::RBNode()
{
    this->color = RED;
}

RBNode::RBNode(RBNode* l, RBNode* r, RBNode* p, int c, int v) :
    left(l),
    right(r),
    parent(p),
    color(c),
    value(v)
{

}

#define NIL &leaf
RBNode leaf = { NIL, NIL, nullptr, BLACK, 0 };


void RBTree::rotateLeft(RBNode* x)
{
    RBNode* y = x->right;
    x->right = y->left;

    if (y->left != NIL)
        y->left->parent = x;

    if (y != NIL)
        y->parent = x->parent;

    if (!x->parent)
    {
        tree = y;
    }
    else
    {
        if (x == x->parent->left)
        {
            x->parent->left = y;
        }
        else
        {
            x->parent->right = y;
        }
    }

    y->left = x;
    x->parent = y;
}

void RBTree::rotateRight(RBNode* y)
{
    RBNode* x = y->left;
    y->left = x->right;

    if (x->right != NIL)
        x->right->parent = y;

    if (x != NIL)
        x->parent = y->parent;

    if (!y->parent)
    {
        tree = x;
    }
    else
    {
        if (y == y->parent->left)
        {
            y->parent->left = x;
        }
        else
        {
            y->parent->right = x;
        }
    }

    x->right = y;
    y->parent = x;
}

void RBTree::rebalance(RBNode* current)
{
    while (current != tree && current->parent->color == RED)
    {
        if (current->parent == current->parent->parent->left)
        {
            RBNode* uncle = current->parent->parent->right;
            if (uncle->color == RED)
            {
                /*
                    If parent node and uncle node are RED both,
                    then they will be repainted in BLACK. In order to preserve tree integrity
                    parent of parent node will be repainted in RED
                    However, the parent of parent node may break the integrity, since it is RED from now,
                    so we should check and fix this
                */
                current->parent->color = BLACK;
                uncle->color = BLACK;
                current->parent->parent->color = RED;
                current = current->parent->parent;
            }
            else
            {
                /*
                    If parent node is RED and uncle node is BLACK
                */
                if (current == current->parent->right)
                {
                    /*
                        If current node is a right child of parent node then
                        make current node be a left child
                    */
                    current = current->parent;
                    rotateLeft(current);
                }

                current->parent->color = BLACK;
                current->parent->parent->color = RED;
                rotateRight(current->parent->parent);
            }
        }
        else
        {
            RBNode* uncle = current->parent->parent->left;
            if (uncle && uncle->color == RED)
            {
                current->parent->color = BLACK;
                uncle->color = BLACK;
                current->parent->parent->color = RED;
                current = current->parent->parent;
            }
            else
            {
                if (current == current->parent->left)
                {
                    current = current->parent;
                    rotateRight(current);
                }

                current->parent->color = BLACK;
                current->parent->parent->color = RED;
                rotateLeft(current->parent->parent);
            }
        }
    }

    tree->color = BLACK;
}

RBTree::RBTree() :
    tree(NIL)
{

}

RBTree::~RBTree()
{

}

RBNode* RBTree::insert(int val)
{
    RBNode* current = tree;
    RBNode* parent = nullptr;
    RBNode* newNode = nullptr;

    while (current != NIL)
    {
        if (val == current->value)
            return current;

        parent = current;

        if (val < current->value)
        {
            current = current->left;
        }
        else
        {
            current = current->right;
        }
    }

    newNode = new RBNode;
    newNode->value = val;
    newNode->parent = parent;
    newNode->left = NIL;
    newNode->right = NIL;

    if (parent)
    {
        if (val < parent->value)
            parent->left = newNode;
        else
            parent->right = newNode;
    }
    else
    {
        tree = newNode;
    }

    rebalance(newNode);

    return newNode;
}


RBNode* RBTree::find(int val)
{
    RBNode* current = tree;
    
    while (current != NIL)
    {
        if (val == current->value)
            return current;

        if (val < current->value)
            current = current->left;
        else
            current = current->right;
    }

    return nullptr;
}

int main()
{
    RBTree tree;
    tree.insert(10);
    tree.insert(5);
    tree.insert(4);
    tree.insert(8);

    RBNode* f = tree.find(4);

    return 0;
}