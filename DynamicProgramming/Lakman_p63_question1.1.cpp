#include <iostream>
#include <assert.h>

bool isStringHasUniqueCharacters(const char* str)
{
	//[b, c, a, c]
	//[2, 3, 1, 3]

	for(int i = 0; str && str[i]; ++i)
	{
		for (int j = i + 1; str[j]; ++j)
		{
			if (str[i] == str[j])
				return false;
		}
	}
	
	return true;
}

int main(int argc, char** argv)
{
	assert(argc >= 2);
	printf("Given string %s\n", argv[1]);

	char* str = argv[1];
	
	bool val = isStringHasUniqueCharacters(str);

	std::cout << std::boolalpha << val << std::endl;

	return 0;
}