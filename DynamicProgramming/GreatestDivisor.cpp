#include <stdio.h>
#include <memory>
#include <string>
#include <iostream>
#include <sstream>
#include <list>

extern "C"
{
    #include "number.h"

}

void print_num(bc_num v)
{
    for (uint32_t i = 0; i < v->n_len; ++i)
    {
        printf("%d", v->n_value[i]);
    }
}

bool isPrime(bc_num num)
{
    bc_num i = bc_new_num(1, 0);
    i->n_value[0] = 2;
    bc_num modResult = nullptr;
    while (true)
    {
        bc_modulo(num, i, &modResult, 0);

        int cr = bc_compare(modResult, _zero_);
        if (cr == 0)
        {
            bc_free_num(&modResult);
            bc_free_num(&i);
            return false;
        }

        bc_add(i, _one_, &i, 0);

//         printf("    ");
//         print_num(i);
//         printf("\n");

        if (!bc_compare(i, num))
            break;
    }

    bc_free_num(&modResult);
    bc_free_num(&i);

    return true;
}

//std::list<bc_num> primes;

uint32_t primes[] =
{
#include "Primes.h"
};

const uint32_t primesCount = sizeof(primes) / sizeof((primes)[0]);

int main()
{
    bc_init_numbers();

    bc_num initialValue = bc_new_num(24, 0);
    char initialValueArray[] = { 3,8,6,7,4,5,3,7,4,7,7,9,1,4,8,4,6,3,7,4,6,0,5,9 };
    memcpy(initialValue->n_value, initialValueArray, 24);

    bc_num tempValue = bc_new_num(1, 0);
    tempValue->n_value[0] = 2;

    bc_num modResult = nullptr;

    while (true)
    {
        bc_modulo(initialValue, tempValue, &modResult, 0);
        if (!bc_compare(modResult, _zero_))
        {
            bc_free_num(&modResult);

            printf("Assumed greatest divisor:");
            print_num(tempValue);
            printf("\n");

            printf("Val:");
            print_num(initialValue);
            printf("\n");

            if (0)//isPrime(tempValue))
            {
                break;
            }
            else
            {
                bc_divide(initialValue, tempValue, &initialValue, 0);
               // bc_num tempValue = bc_copy_num(initialValue);
                
                bc_free_num(&tempValue);
                tempValue = bc_new_num(1, 0);
                tempValue->n_value[0] = 2;
                //bc_add(tempValue, _one_, &tempValue, 0);
            }
        }
        else
        {
            bc_add(tempValue, _one_, &tempValue, 0);
        }

        bc_free_num(&modResult);
    }
    return 0;
}