#include <iostream>
#include <stdio.h>
#include <vector>

std::vector<uint64_t> cache;

uint64_t Fibonacci(uint64_t x)
{
    if (cache[x] == 0)
    {
        if (x == 1 || x == 2)
            cache[x] = 1;
        else 
            cache[x] = Fibonacci(x - 1) + Fibonacci(x - 2);
    }

    return cache[x];
}

int main()
{
    cache.resize(100);

    Fibonacci(60);

    return 0;
}