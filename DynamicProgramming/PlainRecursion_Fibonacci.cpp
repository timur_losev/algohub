#include <stdint.h>

uint64_t Fibonacci(uint64_t x)
{
    if (x <= 2)
        return 1;

    return Fibonacci(x - 1) + Fibonacci(x - 2);
}

int main()
{
    Fibonacci(60);

    return 0;
}