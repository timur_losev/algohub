#include <iostream>
#include <assert.h>
#include <string.h>

bool isPermutation(const char* origin, const char* perm)
{
	int originLen = strlen(origin);
	int permLen = strlen(perm);

	if (originLen != permLen)
		return false;

	//sort

	char c[255] = {0};

	for (int i = 0; i < originLen; ++i)
	{
		c[origin[i]]++;
		c[perm[i]]++;
	}

	for (int i = 0; i < 255; ++i)
	{
		if (c[i] > 0 && (c[i] & 1))
		{
			return false;
		}
	}

	return true;
}

int main(int argc, char** argv)
{
	assert(argc >= 3);
	printf("Given string 1 %s\n", argv[1]);
	printf("Given string 1 %s\n", argv[2]);

	bool val = isPermutation(argv[1], argv[2]);

	std::cout << std::boolalpha << val << std::endl;
}