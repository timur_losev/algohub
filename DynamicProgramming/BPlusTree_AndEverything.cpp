//
//  BPlusTree.cpp
//  AlgoHub
//
//  Created by void on 11/7/15.
//  Copyright © 2015 Timur Losev. All rights reserved.
//

#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <signal.h>
#include <iostream>
#include <iterator>
#include <vector>

class BPlusTree
{
private:
    int m_depth = 0;
public:
    enum NodeType
    {
        NodeInner = 0xDEADBEEF,
        NodeLeaf = 0xC0FFEE
    };

    struct Node
    {
        const NodeType type;

        Node(NodeType t) : type(t){}
    };

    struct LeafNode : Node
    {
        uint32_t numKeys = 0;
        int      keys[5];
        int      values[5];

        LeafNode() : Node(NodeLeaf)
        {

        }

        static LeafNode* create()
        {
            return new LeafNode();
        }
    };

    struct InnerNode: Node
    {
        uint32_t numKeys = 0;
        int keys[5];
        void* children[6];

        InnerNode(): Node(NodeInner)
        {

        }

        static InnerNode* create()
        {
            return new InnerNode();
        }
    };

    Node* root = nullptr;

    // Returns the position where 'key' should be inserted in a leaf node
    // that has the given keys.
    static uint32_t leafPositionFor(int key, const int* keys, uint32_t numKeys)
    {
        uint32_t k = 0;
        while (k < numKeys && keys[k] < key)
        {
            ++k;
        }

        return k;
    }

    static uint32_t innerPositionFor(int key, const int* keys, uint32_t numKeys)
    {
        uint32_t k = 0;
        while (k < numKeys && (keys[k] < k || keys[k] == key))
        {
            ++k;
        }

        return k;
    }

    struct InsertionResult
    {
        Node* left = nullptr, *right = nullptr;
        int key;
    };

    void insertLeaf(LeafNode* node, int key, int val, uint32_t index)
    {
        assert( node && node->type == NodeLeaf );
        assert( node->numKeys < 5 );
        assert( index <= 5 );
        assert( index <= node->numKeys );

        if( (index < 5) && (node->numKeys != 0) && (node->keys[index] == key) )
        {
            // We are inserting a duplicate value.
            // Simply overwrite the old one

            node->values[index] = val;
        }
        else
        {
            // The key we are inserting is unique
            for (uint32_t i = node->numKeys; i > index; --i)
            {
                node->keys[i]= node->keys[i-1];
                node->values[i]= node->values[i-1];
            }

            node->numKeys++;
            node->keys[index]= key;
            node->values[index]= val;
        }

    }

    bool insertLeaf(LeafNode* node, int key, int value, InsertionResult* result)
    {
        assert(node && node->type == NodeLeaf);
        assert(node->numKeys <= 5);

        bool wasSplit = false;

        uint32_t i = leafPositionFor(key, node->keys, node->numKeys);
        if (node->numKeys == 5)
        {
            // The node was full. We must split it
            const uint32_t threshold = (5 + 1) / 2;
            LeafNode *newSibling = LeafNode::create();
            newSibling->numKeys = node->numKeys - threshold;

            for (uint32_t j = 0; j < newSibling->numKeys; ++j)
            {
                newSibling->keys[j] = node->keys[threshold + j];
                newSibling->values[j] = node->values[threshold + j];
            }

            node->numKeys = threshold;

            if (i < threshold)
            {
                insertLeaf(node, key, value, i);
            }
            else
            {
                insertLeaf(newSibling, key, value, threshold - i);
            }

            wasSplit = true;

            // Notify the parent about the split
            result->key = newSibling->keys[0];
            result->left = node;
            result->right = newSibling;
        }
        else
        {
            insertLeaf(node, key, value, i);
        }

        return wasSplit;
    }

    BPlusTree()
    {
        root = LeafNode::create();
    }

    void insert(int key, int val)
    {
        InsertionResult result;
        bool wasSplit = false;
        if (m_depth == 0)
        {
            assert((static_cast<LeafNode*>(root))->type == NodeLeaf);

            wasSplit = insertLeaf(static_cast<LeafNode*>(root), key, val, &result);
        }
        else
        {

        }
    }
};

struct Node
{
    Node* pNext = nullptr;
    int val;
    
    Node(int inVal) : val(inVal) {}
};

int RevertPairsOfElementsInLinkedList()
{
    Node* n = new Node(5);
    n->pNext = new Node(13);
    n->pNext->pNext = new Node(15);
    n->pNext->pNext->pNext = new Node(18);
    n->pNext->pNext->pNext->pNext = new Node(20);
    n->pNext->pNext->pNext->pNext->pNext = new Node(11);
    n->pNext->pNext->pNext->pNext->pNext->pNext = new Node(6);
    n->pNext->pNext->pNext->pNext->pNext->pNext->pNext = new Node(7);
    n->pNext->pNext->pNext->pNext->pNext->pNext->pNext->pNext = new Node(72);

    Node* fixup = nullptr;
    
    for (Node* cur = n; cur != nullptr; cur = cur->pNext)
    {
        if (!cur->pNext) break;
        
        Node* nextNext = cur->pNext->pNext;
        cur->pNext->pNext = cur;
        Node* toFix = cur->pNext;
        cur->pNext = nextNext;
        
        if (fixup)
            fixup->pNext = toFix;
        else
            n = toFix;
        fixup = cur;
    }
    
    for (; n; n = n->pNext) {
        std::cout << n->val << " ";
    }
    
    return 0;
}

int BubbleSort()
{
    //Bubble sort
    
    std::vector<int> a  {93,13,5,46,255,4,7,8};
    int size = a.size();
    int kLagest = 3;
    for (int i = 0; i < kLagest; ++i)
    {
        for (int j = 0 ; j < size - i - 1; ++j)
        {
            if (a[j] > a[j + 1])
            {
                std::swap(a[j+1], a[j]);
            }
        }
    }
    
    
    return 0;
}

// A C++ program that returns true if there is a Pythagorean
// Triplet in a given array.
using namespace std;

// Returns true if there is a triplet with following property
// A[i]*A[i] = A[j]*A[j] + A[k]*[k]
// Note that this function modifies given array
bool isTriplet(std::vector<int>& arr, int n)
{
    // Square array elements
    for (int i=0; i<n; i++)
        arr[i] = arr[i]*arr[i];
    
    // Sort array elements
    sort(arr.begin(), arr.end());
    
    // Now fix one element one by one and find the other two
    // elements
    for (int i = n-1; i >= 2; i--)
    {
        // To find the other two elements, start two index
        // variables from two corners of the array and move
        // them toward each other
        int l = 0; // index of the first element in arr[0..i-1]
        int r = i-1; // index of the last element in arr[0..i-1]
        while (l < r)
        {
            // A triplet found
            if (arr[l] + arr[r] == arr[i])
                return true;
            
            // Else either move 'l' or 'r'
            (arr[l] + arr[r] < arr[i])? l++: r--;
        }
    }
    
    // If we reach here, then no triplet found
    return false;
}



int main()
{
}


