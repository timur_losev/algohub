
#include <iostream>

int main()
{
    unsigned int sum = 0;
    for (unsigned int i = 1; i < 1000; ++i)
    {
        if (i % 3 != 0
            && i % 5 != 0
            && i % 7 != 0)
        {
            sum += i;
        }
    }

    return 0;
}