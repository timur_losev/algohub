//http://www.geeksforgeeks.org/perfect-sum-problem-print-subsets-given-sum/

#include <vector>
#include <unordered_set>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <string>
#include <limits.h>
#include <time.h>

#include <queue>

using namespace std;

typedef std::vector<int> IntVec_t;
typedef std::vector<bool> BoolVec_t;

void printAllSubsets(const IntVec_t& A, int sum)
{
	std::vector<BoolVec_t> dp(A.size());
    
    for (int i = 0; i < A.size(); ++i) {
        dp[i].resize(sum + 1);
        dp[i][0] = true;
    }
    
    // Sum arr[0] can be achived with a singled element
    if (A[0] <= sum)
        dp[0][A[0]] = true;
    
    for (int i = 1; i < A.size(); ++i)
    {
        for (int j = 0; j < sum -1; ++j)
        {
            const int val = A[i];
            
            BoolVec_t& prevDp = dp[i-1];
            bool prevDpJ = prevDp[j];
            if (val <= j)
            {
                dp[i][j] = prevDpJ || prevDp[j - val];
            }
            else
            {
                dp[i][j] = prevDpJ;
            }
            
        }
    }
    
    int k = 10;
}
//http://www.geeksforgeeks.org/dynamic-programming-subset-sum-problem/

bool isSubSetSum(const IntVec_t& A, size_t n, int sum)
{
    // Base Cases
    if (sum == 0)
        return true;
    if (n == 0 && sum != 0)
        return false;
    
    // If last element is greater than sum, then ignore it
    if (A[n - 1] > sum)
        return isSubSetSum(A, n-1, sum);
    
    bool ret = isSubSetSum(A, n - 1, sum) || isSubSetSum(A, n -1 , sum - A[n-1]);
    
    return ret;
}

bool isSubSetSumDyn(const IntVec_t& A, int sum)
{
    int n = A.size();
    // The value of subset[i][j] will be true if there is a
    // subset of set[0..j-1] with sum equal to i
    std::vector<BoolVec_t> subset(n + 1);
    
    for (auto& v : subset)
    {
        v.resize(sum + 1);
    }
    
    // If sum is not 0 and set is empty, then answer is false
    for (int i = 0; i <= n; ++i) {
        subset[i][0] = true;
    }
    
    // Fill the subset table in botton up manner
    for (int i = 1; i <= n; i++)
    {
        int val = A[i - 1];
        for (int j = 1; j <= sum; j++)
        {
            if(j<val)
            {
                subset[i][j] = subset[i - 1][j];
            }
            if (j >= val)
            {
                bool b1 = subset[i - 1][j];
                
                int jsubval = j - val;
                bool b2 = subset[i - 1][jsubval];
                subset[i][j] = b1 || b2;
            }
        }
    }

    
    return subset[n][sum];
}

//given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.
int solution_minimum_int(std::vector<int> &A) {
    
    std::unordered_set<int> set;
    
    for (int v : A)
    {
        set.insert(v);
    }
    
    for (int i = 1; i < A.size() + 2; ++i)
    {
        if (set.find(i) == set.end())
            return i;
    }
    
    return 1;
}

// max counter
std::vector<int> solution_max_counters(int N, std::vector<int> &A)
{
    std::vector<int> retval;
    
    retval.resize(N, 0);
    int lastIncrease = 0;
    int maxCounter = 0;
    for (int i = 0; i < A.size(); ++i)
    {
        if (A[i] == N + 1)
        {
            lastIncrease = maxCounter;
        }
        else
        {
            int index = A[i] - 1;
            retval[index] = std::max(retval[index], lastIncrease);
            retval[index]++;
            maxCounter = std::max(maxCounter, retval[index]);
           
        }
    }
    
    for (int& v : retval) {
        v = std::max(v, lastIncrease);
    }
    
    return retval;
}


std::vector<int> solution_dna(std::string &S, std::vector<int> &P, std::vector<int> &Q)
{
    // write your code in C++14 (g++ 6.2.0)
    std::vector<int> retval;
    
    std::unordered_map<char, int> impacts { {'A', 1}, {'C', 2}, {'G', 3}, {'T' ,4}};
    
    for (int i = 0; i < P.size(); ++i) {
        int min = 5;
        for (int j = P[i]; j <= Q[i] ; ++j) {
            char c = S[j];
            min = std::min(min, impacts[c]);
        }
        
        retval.push_back(min);
    }
    
    return retval;
    
}

int solution_slice_5_3(std::vector<int> &A) {
    
    int min_idx = 0;
    float min_value = 10001;
    
    for (int i = 0; i < A.size(); ++i)
    {
        if( (A[i] + A[i + 1]) / 2.0f < min_value )
        {
            min_idx = i;
            min_value = (A[i] + A[i + 1]) / 2.0f;
        }
        
        if ( (i < (A.size() - 2)) && (A[i] + A[i + 1] + A[i + 2]) / 3.0f < min_value)
        {
            min_idx = i;
            min_value = A[i] + A[i + 1] + A[i + 2] / 3.0f;
        }
    }
    
    return min_idx;
}

int solution_17_1(std::vector<int>& v)
{
	const int possible_rolls = 6;
	int min_value = INT_MIN;

	std::vector<int> sub_solutions(possible_rolls + v.size(), min_value);
	std::fill_n(sub_solutions.begin(), possible_rolls, v[0]);

	for (int i = possible_rolls + 1; i < v.size() + possible_rolls; ++i)
	{
		int max_prev = min_value;

		for (int prev_idx = 0; prev_idx < possible_rolls; ++prev_idx)
		{
			int v = sub_solutions[i - prev_idx - 1];
			max_prev = std::max(max_prev, v);
		}

		sub_solutions[i] = v[i - possible_rolls] + max_prev;
	}

	return sub_solutions[v.size() + possible_rolls - 1];

}

int soulution_fast_leader_of_sorted_array(vector<int>& A)
{
	int n = A.size();
	int leader = -1;

	int candidate = A[n / 2];
	int count = 0;
	
	for (int v : A)
	{
		if (v == candidate)
			count++;
	}

	if (count > n / 2)
		leader = candidate;

	return leader;
}

int solution_find_bug_leader(vector<int>& A)
{
	int n = A.size();
	vector<int> L;
	L.push_back(-1);

	for (int i = 0; i < n; i++)
	{
		L.push_back(A[i]);
	}

	int count = 1;
	int pos = (n / 2) + 1;
	int candidate = L[pos];

	for (int i = 1; i <= n; i++)
	{
		if (L[i] == candidate)
			count = count + 1;
	}

	if (count > pos)
		return candidate;

	return (-1);
}



/*


A pointer is called a linked list if:

        it is an empty pointer (it is then called a terminator or an empty list); or
        it points to a structure (called a node or the head) that contains a value and a linked list (called the tail).

The length of a list is defined as the total number of nodes it contains. In particular, an empty list has length 0.

For example, consider the following linked list:
  A -> B -> C -> D ->

This list contains four nodes: A, B, C and D. Node D is the last node and its tail is the terminator. The length of this list is 4.

Assume that the following declarations are given:

    struct IntList {
      int value;
      IntList * next;
    };

Write a function:

    int solution(IntList * L);

that, given a non-empty linked list L consisting of N nodes, returns its length.

For example, given list L shown in the example above, the function should return 4.

Assume that:

        N is an integer within the range [1..5,000];
        list L does not have a cycle (each non-empty pointer points to a different structure).

In your solution, focus on correctness. The performance of your solution will not be the focus of the assessment.

*/

struct IntList {
	int value = 0;
	IntList * next = nullptr;
};
int solution_for_lists(IntList * L) {
	
	int size = 0;
		
	for (IntList* p = L; p != nullptr; p = p->next)
	{
		size++;
	}

	return size;
}

int solution_find_a_bug_tom_tom(vector<int> & A)
{
	int n = A.size();
	int result = 0;

	for (int i = 0; i < n - 1; i++) 
	{
		if (A[i] == A[i + 1])
			result = result + 1;
	}

	int r = 0;

	for (int i = 0; i < n; i++)
	{
		int count = 0;
		if (i > 0)
		{
			if (A[i - 1] != A[i])
			{
				count = count + 1;
			}
			else
			{
				count = count - 1;
			}
		}
		if (i < n - 1)
		{
			if (A[i + 1] != A[i])
			{
				count = count + 1;
			}
			else
			{
				count = count - 1;
			}
		}
		r = std::max(r, count);
	}
	return result + ((r == 0 && A.size() > 1) ? -1 : r);
}

struct tree 
{
	tree(int inX) : x(inX) {}
	int x = 0;
	tree * l = nullptr;
	tree * r = nullptr;
};

int countVisibleNodes(tree* inTree, int maxValue)
{
	if (inTree == nullptr)
	{
		return 0;
	}

	int num = 0;

	if (inTree->x >= maxValue)
	{
		num = 1;
		maxValue = inTree->x;
	}

	return num + countVisibleNodes(inTree->l, maxValue) + countVisibleNodes(inTree->r, maxValue);
}

#include <stack>

struct StackElem
{
	tree* node = nullptr;
	int maxValue = 0;
    
    StackElem(tree*, int){}
};

int countVisNodesIter(tree* inTree)
{
	if (inTree == nullptr)
		return 0;

	std::stack<StackElem> stack;
	stack.push({ inTree, inTree->x });
	int count = 0;

	while (!stack.empty())
	{
		StackElem current = stack.top(); stack.pop();
		if (current.node->x >= current.maxValue)
		{
			//This is a visible node.
			count++;
		}

		int maxValue = std::max(current.node->x, current.maxValue);
		if (current.node->l != nullptr)
		{
			stack.push({ current.node->l, maxValue });
		}
		if (current.node->r != nullptr)
		{
			stack.push({ current.node->r, maxValue });
		}

	}
	return count;
}

int findVisibleNodes(tree* inTree)
{
	return countVisibleNodes(inTree, INT_MIN);
}

//Given a integer as a input and replace all the ‘0’ with ‘5’ in the integer.
int replace_0_with_5(int v)
{
    int retval = v;
    for(int i = 1; v!=0; ++i)
    {
        int m = v % 10;
        if (m == 5)
        {
            
        }
    }
    
    return 0;
}

//Given a 0 - 5 random int generator create a 0 - 7 random int generator.

int rand_0_5()
{
	int vals[5][5] = 
	{
		{ 0,1,2,3,4 },
		{ 5,6,0,1,2 },
		{ 3,4,5,6,0 },
		{ 1,2,3,4,5 },
		{ 6,-1,-1,-1,-1 }
	};
	
	int result = -1;
	while (result == -1)
	{
		int i = rand() % 5;
		int j = rand() % 5;

		result = vals[i][j];
	}

	return result;
}

void treeBSF(tree* root)
{
	std::queue<tree*> stack;
	stack.push(root);
	while (!stack.empty())
	{
		tree* x = stack.front();
		stack.pop();
		std::cout << x->x << " ";
		if (x->l)
			stack.push(x->l);
		if (x->r)
			stack.push(x->r);
	}
}

int main()
{
	srand(time(nullptr));

	for (int i = 0; i < 100; ++i)
	{
		std::cout << rand_0_5() << std::endl;
	}

	tree* root = new tree(8);
	root->l = new tree(2);
	root->l->l = new tree(8);
	root->l->r = new tree(7);
	root->r = new tree(6);
	root->r->r = new tree(1);
	root->r->l = new tree(15);

	int visNodes = findVisibleNodes(root);

	treeBSF(root);

	return 0;
}
