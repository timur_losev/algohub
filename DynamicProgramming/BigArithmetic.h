#pragma once

#include <stdint.h>
#include <memory.h>
#include <vector>
#include <assert.h>

class BigInt
{
public:
    typedef int32_t valueType;
private:
    std::vector<valueType> m_data;
public:
    static const uint32_t base = 10;

    BigInt(size_t size)
    {
        m_data.resize(size);
    }

    BigInt(const std::vector<valueType> data)
    {
        m_data = data;
    }

    ~BigInt()
    {
        
    }

    const std::vector<valueType> getData() const
    {
        return m_data;
    }

    const valueType operator[](int index) const
    {
        return m_data[index];
    }

    valueType& operator[](int index)
    {
        return m_data[index];
    }

    BigInt operator*(const BigInt& B) const
    {
        assert(B.m_data.size() == m_data.size());
        BigInt C(m_data.size());


    }

    BigInt operator*(valueType n) const
    {
        int32_t carry = 0;

        BigInt B(m_data.size());

        for (uint32_t i = 0; i < m_data.size(); ++i)
        {
            B[i] = n * m_data[i];
            B[i] += carry;

            if (B[i] >= base)
            {
                carry = B[i] / base;
                B[i] %= base;
            }
            else
            {
                carry = 0;
            }
        }

        assert("overflow in MUL!" && carry == 0);
    }

    BigInt operator+(const BigInt& B) const
    {
        assert(B.m_data.size() == m_data.size());
        
        int32_t sum = 0;
        int32_t carry = 0;

        const BigInt& A = *this;

        BigInt C(m_data.size());

        for (uint32_t i = 0; i < m_data.size(); ++i)
        {
            sum = A[i] + B[i] + carry;

            if (sum >= base)
            {
                carry = 1;
                sum -= base;
            }
            else
            {
                carry = 0;
            }

            C[i] = sum;
        }

        assert("overflow in addition!" && carry == 0);

        return C;
    }
};