#include <iostream>
#include <assert.h>
#include <string.h>

char* strreverse(char* str, int len)
{
	int half = len >> 1;

	for (int i = 0; i < half; ++i)
	{
		std::swap(str[i], str[len - 1 - i]);
	}

	return str;
}

int main(int argc, char** argv)
{
	assert(argc >= 2);
	printf("Given string %s\n", argv[1]);

	char* str = argv[1];
	int len = strlen(str);

	strreverse(str, len);

	printf("%s\n", str);

	return 0;	
}