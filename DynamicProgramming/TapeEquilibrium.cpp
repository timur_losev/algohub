/*
    A non-empty zero-indexed array A consisting of N integers is given. Array A represents numbers on a tape.

    Any integer P, such that 0 < P < N, splits this tape into two non-empty parts: A[0], A[1], ..., A[P ? 1] and A[P], A[P + 1], ..., A[N ? 1].

    The difference between the two parts is the value of: |(A[0] + A[1] + ... + A[P ? 1]) ? (A[P] + A[P + 1] + ... + A[N ? 1])|

    In other words, it is the absolute difference between the sum of the first part and the sum of the second part.

    For example, consider array A such that:
    A[0] = 3
    A[1] = 1
    A[2] = 2
    A[3] = 4
    A[4] = 3

    We can split this tape in four places:

    P = 1, difference = |3 - 10| = 7
    P = 2, difference = |4 - 9| = 5
    P = 3, difference = |6 - 7| = 1
    P = 4, difference = |10 - 3| = 7

    Write a function:

    int solution(vector<int> &A);

    that, given a non-empty zero-indexed array A of N integers, returns the minimal difference that can be achieved.

    For example, given:
    A[0] = 3
    A[1] = 1
    A[2] = 2
    A[3] = 4
    A[4] = 3

    the function should return 1, as explained above.

    Assume that:

    N is an integer within the range [2..100,000];
    each element of array A is an integer within the range [-1,000..1,000].

    Complexity:

    expected worst-case time complexity is O(N);
    expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).

    Elements of input arrays can be modified.

*/

#include <iostream>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <thread>
#include <memory>
#include <mutex>

int check(const std::vector<int>& a)
{
    int sum1 = a[0];
    int retval = INT_MAX;
    for (int i = 1; i < a.size(); ++i)
    {
        int sum2 = 0;
        for (int j = i; j < a.size(); ++j)
        {
            sum2 += a[j];
        }

        int dif = sum1 - sum2;
        if (dif < retval)
            retval = dif;

        sum1 += a[i];
    }

    return retval;
}

int proc(const std::vector<int> a)
{
    if (a.size() < 2)
    {
        return -1;
    }
    else
        if (a.size() == 2)
        {
            return std::abs(a[1] - a[2]);
        }
        else
        {
            int sum1 = a[0], sum2 = a[a.size() - 1];
            int retval = INT_MAX;
            for (int i = 1, j = a.size() - 2; j >= i; )
            {
                if (sum1 <= sum2)
                {
                    sum1 += a[i++];
                }
                else
                {
                    sum2 += a[j--];
                }

            }

            return std::abs(sum1 - sum2);
        }
}

int main()
{
    int m = check({ -10,15,12,-20,14 });
    m = proc({ -10,15,12,-20,14 });

    srand(time(0));

    std::shared_ptr<std::thread> tpool[1];

    int tid = 0;
    for (auto& t : tpool)
    {
        t.reset(new std::thread([tid]
        {
            int tries = 12;
            while (tries--)
            {
                int N = (rand() % (100000 - 2)) + 2;
                std::vector<int> v;
                v.resize(N);
                for (int i = 0; i < N; ++i)
                {
                    int sign = rand() % 2;
                    int value = rand() % 1000;
                    v[i] = sign == 0 ? value : -value;
                }

                int ret = proc(v);
                int ch = check(v);

                assert(ret == ch);

                printf("[%d]Try #%d\n", tid, 12 - tries);
            }
        }));

        ++tid;
    }

    while (true)
    {
        // instead of join all threads
    }


    return 0;
}