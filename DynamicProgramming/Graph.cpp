#include <vector>
#include <unordered_set>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <string>
#include <limits.h>
#include <time.h>
#include <list>

#include <queue>
#include <stack>

class Graph
{
private:
	int V = 0;
	std::vector<std::list<int>> adj;

	void _DSF(int v, std::vector<bool> & visited)
	{
		visited[v] = true;

		std::cout << v << " ";

		const std::list<int>& vrt = adj[v];
		for (int i : vrt)
		{
			if (!visited[i])
			{
				_DSF(i, visited);
			}
		}
	}
public:

	Graph(int inV) :
		V(inV)
	{
		adj.resize(V);
	}

	void addEdge(int v, int w)
	{
		adj[v].push_back(w);
	}

	// DFS traversal of the vertices
	// reachable from v
	void DSF(int v)
	{
		std::vector<bool> visited(V, false);

		_DSF(v, visited);
	}


	void DSFIter(int v)
	{
		std::vector<bool> visited(V, false);

		std::stack<int> stack;

		stack.push(v);

		while (!stack.empty())
		{
			int frame = stack.top(); stack.pop();
			visited[frame] = true;

			std::cout << frame << " ";

			const std::list<int>& vrt = adj[frame];

			for (int i : vrt)
			{
				if (!visited[i])
				{
					stack.push(i);
				}
			}
		}
	}

	void BFS(int v)
	{
        std::vector<bool> visited(V, false);
        
        std::queue<int> stack;
        
        stack.push(v);
        
        while (!stack.empty())
        {
            int frame = stack.front(); stack.pop();
            visited[frame] = true;
            
            std::cout << frame << " ";
            
            const std::list<int>& vrt = adj[frame];
            
            for (int i : vrt)
            {
                if (!visited[i])
                {
                    stack.push(i);
                }
            }
        }
	}
};

int main()
{
	Graph g(4);

	g.addEdge(0, 1);
	g.addEdge(0, 2);
	g.addEdge(1, 2);
	g.addEdge(2, 0);
	g.addEdge(2, 3);
	g.addEdge(3, 3);

	g.DSF(2);
	std::cout << std::endl;
	g.DSFIter(2);

	return 0;
}
