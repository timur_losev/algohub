//
//  AmazonPrep.cpp
//  AlgoHub
//
//  Created by void on 10/14/17.
//  Copyright © 2017 Timur Losev. All rights reserved.
//

#include "AmazonPrep.hpp"
#include <vector>
#include <unordered_set>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <string>
#include <limits.h>
#include <time.h>
#include <list>

#include <queue>
#include <stack>
#include <unordered_set>
#include <unordered_map>
#include <assert.h>

//- Find if there are two elements in an int array that sum up to a given x
bool solution_if_2_elements_sum_up_to_x(const std::vector<int>& a, int x)
{
    std::unordered_set<int> complements;
    
    for (int i : a)
    {
        int v = x - i;
        auto retval = complements.find(v);
        if (retval != complements.end())
            return true;
        complements.insert(i);
    }
    
    return false;
}

void enum_all_possible_pairs(const std::vector<int>& a)
{
    
}

//3. Write an algorithm that returns an array with the integers that occur an odd number of times in the array. Now optimize this algorithm.

std::vector<int> odd_elements(const std::vector<int>& a)
{
    std::unordered_map<int, int> set;
    for (int i : a) {
        set[i]++;
    }
    
    std::vector<int> retval;
    for (auto& p: set)
    {
        if ((p.second % 2) != 0)
        {
            retval.push_back(p.first);
        }
    }
    
    return retval;
}

//Write an algorithm that returns an array containing the multiplication of all integers - except the one on the same index - in the array. What is the complexity of the algorithm? Optimize the algorithm to O(n). Write the algorithm without division.

std::vector<int> multiplications_of_all_except_current(const std::vector<int>& a)
{
    std::vector<int> retval(a.size());
    int product = 1;
    for (int i : a) {
        product *= i;
    }
    
    for (int i = 0; i < a.size(); ++i) {
        retval[i] = product / a[i];
    }
    
    return retval;
}

// find max without if-else
int uncond_max(int x, int y)
{
    //x = 1
    //y = 2
    //
    //x = 2
    //y = 1
    return 0;
}

// 1 - 1
// 2 - 2
// 3 - 6
// 4 - 24
// 5 - 120
// n*n!
std::vector<std::string> get_all_string_permutation(const std::string& str)
{
    std::vector<std::string> retval;
    std::string one;
    one.push_back(str[0]);
    retval.push_back(one);
    one.clear();
    
    for (int i = 1; i < str.size(); ++i) {
        std::vector<std::string> temp;
        for (int j = 0, e = retval.size(); j < e; ++j)
        {
            std::string cur = retval[j];
            for (int s = 0; s < cur.size() + 1; ++s)
            {
                std::string tempstr = cur;
                one.push_back(str[i]);
                
                tempstr.insert(s, one);
                temp.push_back(tempstr);
                
                one.clear();
            }
        }
        retval.swap(temp);
    }
    
    return retval;
}

int get_peak_helper(const std::vector<int>& a, int i, int j)
{
    int m = i + (j-i)/2;
    int k = i;
    
    int v = a[m];
    if (v >= a[m-1] && v >= a[m+1])
    {
        return v;
    }
    else if (v < a[m+1])
    {
        k = j;
    }
    
    if (k < m)
        std::swap(k, m);
    
    return get_peak_helper(a, m, k);
    
}

int get_peak(const std::vector<int>& a)
{
    return get_peak_helper(a, 0, a.size());
}

void selection_sort(std::vector<int>& a)
{
    for (int i = 0; i < a.size() - 1; ++i)
    {
        int smalest = i;
        for (int j = i + 1; j < a.size(); ++j)
        {
            if (a[j] < a[smalest])
            {
                smalest = j;
                std::swap(a[i], a[smalest]);
            }
        }
    }
}

//[50, 9, 1, 2]
//95021
//50,90,10,20

bool cmp(const std::string& x, const std::string& y)
{
    const int ix = atoi(x.c_str());
    const int iy = atoi(y.c_str());
    
    return ix > iy;
}

bool compare(const std::string& x, const std::string& y)
{
    const int deltaLen = x.length() - y.length();
    
    if (deltaLen == 0)
    {
        return cmp(x, y);
    }
    else if (deltaLen < 0)
    {
        std::string alignX = x;
        std::copy(y.begin(), y.end() + deltaLen, std::back_inserter(alignX));
        return cmp(alignX, y);
    }
    else
    {
        std::string alignY = y;
        std::copy(x.begin(), x.begin() + deltaLen, std::back_inserter(alignY));
        return cmp(x, alignY);
    }
    
}

int composeMaxNumber(const std::vector<int>& a)
{
    std::vector<std::string> toStrings;
    for (int i : a)
    {
        char buf[16];
        sprintf(buf, "%d", i);
        toStrings.push_back(buf);
    }
    
    std::sort(toStrings.begin(), toStrings.end(), [](const std::string& x, const std::string& y)
              {
                  return compare(x, y);
              });
    
    return 0;
}

int main()
{
    std::vector<int> vvv{1, 10, 11, 12};
    composeMaxNumber(vvv);
    
    std::vector<int> toSort{45,4,32,5,66,13};
    selection_sort(toSort);
    
    std::vector<int> peaks{1,2,3,4,5,2,1};
    
    int peak = get_peak(peaks);
    
    auto ret = get_all_string_permutation("abcde");
    
    std::unordered_set<std::string> check_set;
    for (auto& str: ret) {
        auto check_pair = check_set.insert(str);
        
        assert(check_pair.second != false);
    }
    
    
    std::vector<int> a {1, 7, 3, 2};
    bool b = solution_if_2_elements_sum_up_to_x(a, 4);
    
    std::vector<int> bb {1,3,3,4,1,1,4,3};
    auto v = odd_elements(bb);
    
    //1, 3, 9, 36, 36, 36, 144, 432
    
    //432, 144, 144, 108, 432, 432, 108, 144
    
    v = multiplications_of_all_except_current(bb);
    
    
    int i = 0;
}
