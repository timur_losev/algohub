#include <iostream>
#include <stdio.h>
#include <vector>

/*������� ��� ����� ��������� : 1, 1, 2, 3, 5, 8, 13, 21, 
... (������ ��������� ���� ���� ����� ����� ���� ����������, ���������� ��� � ���� ������).

����� ����� ������ ����� ����, ������� ������ ��������� � ����������� �� �������� ��������.
*/

std::vector<uint64_t> cache;

void Fibonacci(uint64_t x)
{
    for (uint64_t i = 0; i < x; ++i)
    {
        if (i <= 1)
        {
            cache[i] = 1;
        }
        else
        {
            cache[i] = cache[i - 1] + cache[i - 2];
        }
    }
}

int main()
{
    cache.resize(100);
    //cache[0] = 1;
    Fibonacci(100);

    uint64_t val = 0;
    for (uint64_t i = 0; i < cache.size(); ++i)
    {
        if (cache[i] >= 1000000000)
            break;

        if ((i + 1) % 2 != 0)
        {
            val += cache[i];
        }
    }

    return 0;
}