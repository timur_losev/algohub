#pragma once

struct RBNode
{
    RBNode* left = nullptr;
    RBNode* right = nullptr;
    RBNode* parent = nullptr;
    int color;
    int value;

    RBNode();

    RBNode(RBNode* l, RBNode* r, RBNode* p, int c, int v);
};

class RBTree
{
    RBNode* tree = nullptr;

    void rotateLeft(RBNode* x);

    void rotateRight(RBNode* y);

    void rebalance(RBNode* current);
public:

    RBTree();
    ~RBTree();

    RBNode* insert(int val);

    RBNode* find(int val);
};
