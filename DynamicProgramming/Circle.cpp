
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>

#ifndef M_PI
#define M_PI       3.14159265358979323846 
#endif

constexpr double kPrecision = 0.0001;
double solution(int x1, int y1, int r1, int x2, int y2, int r2);
double area(double radius);

struct vec2 {
	double x, y;
	template<typename T>
	vec2(T _x, T _y) : x(_x), y(_y) {}
	double lengthSquared() const { return x*x + y*y; }
};

vec2 operator-(const vec2& a, const vec2& b) {
	return { a.x - b.x, a.y - b.y };
}


void check(int x1, int y1, int r1, int x2, int y2, int r2, double result, std::string comment = {}) {
	const bool passed = std::abs(solution(x1, y1, r1, x2, y2, r2) - result) < kPrecision;
	if (passed)
		std::cout << "passed " << comment << std::endl;
	else
		std::cout << "failed " << comment << " " << solution(x1, y1, r1, x2, y2, r2) << std::endl;
}
int main() {
// 	check(2, 2, 3, 5, 5, 3, 5.1371, "example");
// 	check(0, 0, 2, 0, 0, 2, 12.566370614359173, "same origin, same size");
// 	check(0, 0, 4, 0, 0, 2, 12.566370614359173, "same origin, different size");
// 	check(2, 2, 2, 10, 10, 2, 0, "too far");
// 	check(0, 0, 2, 0, 4, 2, 0, "touching");
// 	check(0, 0, 2, 0, 1, 1, 3.141592653589793, "touching on the inside");
// 	check(2, 2, -3, 5, 5, -3, 5.1371, "negative radius");
	check(14410, -17462, 194105, 72610, 12441, 184105, 87506947419.292, "giant radius1");
	check(11, 20, 648305, 40, 60, 648302, 1320339133570.076, "giant radius");

	system("pause");

	return 0;
}

double solution(int x1, int y1, int r1, int x2, int y2, int r2) {
	// Corner cases
	r1 = abs(r1); // Why would anyone pass negative radius?
	r2 = abs(r2);
	const double minRadius = std::min(r1, r2);
	const double maxRadius = std::max(r1, r2);
	if (x1 == x2 && y1 == y2) {
		// Centers are aligned
		return area(minRadius);
	}

	const vec2 a{ x1, y1 };
	const vec2 b{ x2, y2 };
	const double maxDistance = r1 + r2;
	const double maxDistanceSquared = maxDistance * maxDistance;
	const double distanceSquared = (a - b).lengthSquared();
	if (distanceSquared >= maxDistanceSquared)
	{
		// Either have single intersection point or no intersection at all
		return 0;
	}

	const double distance = std::sqrt(distanceSquared);
	if (distanceSquared <= maxRadius - minRadius) {
		// One circle is inscribed into another one
		return area(minRadius);
	}

	// Are have intersection
	const double minRadiusSquared = minRadius * minRadius;
	const double maxRadiusSquared = maxRadius * maxRadius;

	const double part1 = minRadiusSquared * std::acos((distanceSquared + minRadiusSquared - maxRadiusSquared) /	(2 * distance * minRadius));

	const double part2 = maxRadiusSquared * std::acos((distanceSquared + maxRadiusSquared - minRadiusSquared) /
		(2 * distance * maxRadius));
	const double part3 = 0.5 * std::sqrt((minRadius + maxRadius - distance) *
		(distance + minRadius - maxRadius) *
		(distance - minRadius + maxRadius) *
		(distance + minRadius + maxRadius));

	return part1 + part2 - part3;
}

double area(double radius) {
	return M_PI * radius * radius;
}