#include <stdio.h>
#include <memory>
#include <string>
#include <iostream>
#include <sstream>
#include <list>

extern "C"
{
#include "number.h"
}

void print_num(const char* name, bc_num v)
{
    printf(name);
    for (uint32_t i = 0; i < v->n_len; ++i)
    {
        printf("%d", v->n_value[i]);
    }
    printf("\n");
}

bool check(bc_num num)
{
    bc_num i = bc_new_num(3, 0);
    i->n_value[0] = 1;
    i->n_value[1] = 0;
    i->n_value[2] = 0;

    bc_num modResult = nullptr;
    bc_num divResult = nullptr;
    while (i->n_len == 3)
    {
        bc_divmod(num, i, &divResult, &modResult, 0);

        int cr = bc_compare(modResult, _zero_);
        if (cr == 0 && divResult->n_len == 3)
        {
            print_num(" Divisor ", i);
            print_num(" Divided ", num);

            bc_free_num(&divResult);
            bc_free_num(&modResult);
            bc_free_num(&i);            
            return true;
        }

        bc_add(i, _one_, &i, 0);

        if (!bc_compare(i, num))
            break;
    }

    bc_free_num(&divResult);
    bc_free_num(&modResult);
    bc_free_num(&i);

    return false;
}


int main()
{
    bc_init_numbers();
    bc_num val = bc_new_num(6, 0);
    char valArr[] = { 9,9,8,0,0,1 };
    memcpy(val->n_value, valArr, 6);

    while (true)
    {
        bc_sub(val, _one_, &val, 0);
        if (
            val->n_value[0] == val->n_value[val->n_len - 1] &&
            val->n_value[1] == val->n_value[val->n_len - 2] &&
            val->n_value[2] == val->n_value[val->n_len - 3] && check(val)
            )
        {
                print_num("Value ", val);
                break;
        }
//         else
//         {
//             print_num("Value2 ", val);
//         }
    }

    return 0;
}