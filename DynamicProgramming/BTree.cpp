
#include <iostream>
#include <array>

struct BNode
{
    bool isLeaf = true;
    int value[5] = {0};
    std::array<BNode*, 6> child;
    int filledChild = 0;
};

BNode* current = nullptr;
BNode* root = nullptr;

void sort(BNode* node)
{
    for (uint32_t i = 0; i < node->filledChild; ++i)
    {
        for (uint32_t j = i; j <= node->filledChild; ++j)
        {
            if (node->value[i] > node->value[j])
            {
                std::swap(node->value[i], node->value[j]);
            }
        }
    }
}

int split(BNode* node, int middle)
{
    if (middle == -1)
    {
        int mid = node->value[2];
        node->value[2] = 0;
        node->filledChild--;

        BNode* newNode1 = new BNode();
        node->isLeaf = true;

        for (uint32_t i = 3; i < 5; ++i)
        {
            newNode1->value[i - 3]  = node->value[i];
            newNode1->child[i - 3]  = node->child[i];
            newNode1->filledChild++;
            node->value[i] = 0;
            node->filledChild--;
        }

        for (uint32_t i = 0; i < 6; ++i)
        {
            node->child[i] = 0;
        }

        BNode* newNode2 = new BNode();
        newNode2->isLeaf = false;

        newNode2->value[0] = mid;
        newNode2->child[0] = node;
        newNode2->child[1] = newNode1;
        newNode2->filledChild++;
        root = newNode2;

        return mid;
    }
    else
    {

    }

    return 0;
}

void insert(int a)
{
    if (!root)
    {
        root = new BNode();
        current = root;
    }

    if (current->isLeaf && current->filledChild == 5)
    {
        int temp = split(current, -1);

        uint32_t i;
        for (i = 0; i < current->filledChild; ++i)
        {
            if (a > current->value[i] && a < current->value[i + 1])
            {
                ++i; // i-th child is ok for inserting
                break;
            }
            else if (a < current->value[0])
            {
                break; // current node is ok for inserting
            }
        }

        current = current->child[i];
    }
    else
    {
        uint32_t i = 0;

        while (!current->isLeaf)
        {
            for (i = 0; i < current->filledChild; ++i)
            {
                if (a > current->value[i] && a < current->value[i + 1])
                {
                    ++i;
                    break;
                }
                else if (a < current->value[0])
                {
                    break;
                }
            }

            if (current->child[i]->filledChild == 5)
            {

            }
            else
            {
                current = current->child[i];
            }
        }
    }

    current->value[current->filledChild] = a;
    sort(current);
    current->filledChild++;

}

int main()
{
    insert(120);
    insert(89);
    insert(64);
    insert(45);
    insert(34);
    insert(31);

    return 0;
}