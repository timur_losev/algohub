//
//  GetMaximumPermurationValue.cpp
//  AlgoHub
//
//  Created by void on 10/23/17.
//  Copyright © 2017 Timur Losev. All rights reserved.
//

#include <algorithm>
#include <string>
#include <vector>
#include <assert.h>
#include <iostream>
#include <set>
#include <unordered_map>
#include <iterator>
#include <time.h>

#include "GetMaximumPermurationValue.hpp"

struct TestInput
{
    std::vector<int> input;
    std::string expectedResult;
};

// O(mn log(n) )
std::string GetMaximumPermutationValue(const std::vector<int>& inArr)
{
    // Converting the input array to strings because
    //      A) it doesn't really impact on the performance
    //      B) the output's type must be string in order to prevent from int overflow

    StringVector_t toStrings = ArrayToStringVector(inArr);

    std::sort(toStrings.begin(), toStrings.end(), [](const std::string& x, const std::string& y)
    {
		auto cmp = [](const std::string& x, const std::string& y)->bool
		{
			return std::lexicographical_compare(y.begin(), y.end(), x.begin(), x.end());
		};

		const int deltaLen = static_cast<int>(x.length()) - static_cast<int>(y.length());

		if (deltaLen == 0)
		{
			return cmp(x, y);
		}
		else if (deltaLen < 0)
		{
			std::string alignX = x;
			std::copy(y.begin(), y.end() + deltaLen, std::back_inserter(alignX));
			return cmp(alignX, y);
		}
		else
		{
			std::string alignY = y;
			std::copy(x.begin(), x.begin() + deltaLen, std::back_inserter(alignY));
			return cmp(x, alignY);
		}
    });

    std::string retValue = StringVectorToString(toStrings);

    return retValue;
}

// Brute force for test purposes.
// Recursive because I constrained the possible array's size :)
// O(n!)
/////////////////////////////////////////////////////////////
void GetAllPermutationsR(std::vector<StringVector_t>& retval, StringVector_t& str, int l, int r)
{
    int i;
    if (l == r)
    {
        retval.push_back(str);
    }
    else
    {
        for (i = l; i <= r; i++)
        {
            std::swap(str[l], str[i]);
            GetAllPermutationsR(retval, str, l+1, r);
            std::swap(str[l], str[i]);
        }
    }
};

std::vector<StringVector_t> GetAllPermutations(const std::vector<int>& inArr)
{
    StringVector_t str = ArrayToStringVector(inArr);
    
    std::vector<StringVector_t> retval;
    
    GetAllPermutationsR(retval, str, 0, static_cast<int>(str.size()) - 1);
    
    return retval;
}

std::string GetMaximumPermutationValue_BruteForce(const std::vector<int>& inArr)
{
    std::vector<StringVector_t> perm = GetAllPermutations(inArr);
    
    //0 to 18,446,744,073,709,551,615
    uint64_t max = 0;
    for (auto& v : perm)
    {
        std::string str = StringVectorToString(v);
        
        assert(str.size() <= 20);
        
        uint64_t value = std::strtoull(str.c_str(), nullptr, 0);
        
        if (value > max)
            max = value;
    }
    
    return std::to_string(max);
}
/////////////////////////////////////////////////////////////




void generateSamples(std::vector<TestInput>& inOutVec, int inHowMany)
{
    srand(time(nullptr));
    
    // I like hard coded range here
    const int minRange = 4;
    const int maxRange = 6;
    for (int i = 0; i < inHowMany; ++i)
    {
        int range = rand() % (maxRange - minRange + 1) + minRange;
        
        TestInput toAdd;
        std::vector<int> randomizedInput;
        randomizedInput.reserve(range);
        while (range--)
        {
            randomizedInput.push_back(rand() % 201 + 1);
        }
        
        toAdd.input = randomizedInput;
        toAdd.expectedResult = GetMaximumPermutationValue_BruteForce(randomizedInput);
        
        inOutVec.push_back(toAdd);
    }
}

int main()
{
    std::vector<TestInput> test
    {
        {
            {199,22,201,10,63}, "632220119910"
        },
        {
            {50, 9, 1, 2}, "95021"
        },
        {
            {1, 10, 11, 12}, "1211110"
        },
    };
    
    std::cout << "Generating test samples" << std::endl;
    
    // generate random samples
    generateSamples(test, 100);
    
    std::cout << "Starting test" << std::endl;
    
    for (const auto& testTier : test)
    {
        std::string actualResult = GetMaximumPermutationValue(testTier.input);
        if (actualResult != testTier.expectedResult)
        {
            std::cout << "!!!!!!TEST FAILED : Actual " << actualResult << " but expected " << testTier.expectedResult << " Array: ";
            for (int v : testTier.input) {
                std::cout << v << ",";
            }
            std::cout << std::endl;
        }
        else
        {
            std::cout << "TEST PASSED : " << actualResult << std::endl;
        }
        
        
    }
    
    return 0;
}
