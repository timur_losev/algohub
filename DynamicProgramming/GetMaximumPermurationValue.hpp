//
//  GetMaximumPermurationValue.hpp
//  AlgoHub
//
//  Created by void on 10/23/17.
//  Copyright © 2017 Timur Losev. All rights reserved.
//

#ifndef GetMaximumPermurationValue_hpp
#define GetMaximumPermurationValue_hpp

typedef std::vector<std::string> StringVector_t;

// Utility
std::string StringVectorToString(const StringVector_t& inArr)
{
    std::string retValue;
    std::for_each(inArr.begin(), inArr.end(), [&retValue](const std::string& v)
    {
        retValue += v;
    });

    return retValue;
}

StringVector_t ArrayToStringVector(const std::vector<int>& inArr)
{
    StringVector_t toStrings;
    for (int i : inArr)
    {
        char buf[16];
        sprintf(buf, "%d", i);
        toStrings.push_back(buf);
    }

    return toStrings;
}

#endif /* GetMaximumPermurationValue_hpp */
