#include <iostream>

uint32_t EuclideGCD(uint32_t m, uint32_t n)
{
    if (m < n)
    {
        std::swap(m, n);
    }

    uint32_t r = m % n;

    while (r > 0)
    {
        m = n;
        n = r;

        r = m % n;
    }

    return n;
}

int main()
{
    uint32_t nod = EuclideGCD(119, 544);
    nod = EuclideGCD(89, 85);


    return 0;
}