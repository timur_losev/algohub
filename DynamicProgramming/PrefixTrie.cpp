#include <vector>
#include <unordered_set>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <string>
#include <limits.h>
#include <time.h>
#include <list>

#include <queue>
#include <stack>
#include <locale>

struct node
{
	std::vector<node*> children;

	// isEndOfWord is true if the node represents
	// end of a word
	bool isEndOfWorld = false;

	node()
	{
		children.resize(26, nullptr);
	}
};

void insert(node* root, const std::string& key)
{
	node* crawl = root;

	for (char k : key)
	{
		// index normalization
		int index = tolower(k) - 'a';
		if (!crawl->children[index])
		{
			crawl->children[index] = new node();
		}

		crawl = crawl->children[index];
	}

	crawl->isEndOfWorld = true;
}

bool search(node* root, const std::string& key)
{
	node* crawl = root;

	for (char k : key)
	{
		// index normalization
		int index = tolower(k) - 'a';
		if (!crawl->children[index])
		{
			return false;
		}

		crawl = crawl->children[index];
	}

	return crawl && crawl->isEndOfWorld;
}

int main()
{
	std::vector<std::string> phonebook =
	{
		"Abba",
		"Alice",
		"Bob"
	};

	node* root = new node();
	insert(root, phonebook[0]);
	insert(root, phonebook[1]);
	insert(root, phonebook[2]);


	search(root, "Abb");
}
