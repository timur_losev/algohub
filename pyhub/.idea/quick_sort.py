if __name__ == '__main__':
    import quick_sort
    raise SystemExit(quick_sort.main())

def quickSortR(A, K, N):
    i = K
    j = N - 1
    P = A[(K + N) >> 1]

    while True:
        while A[i] < P:
            i += 1
        while A[j] > P:
            j -= 1;

        if i <= j:
            A[i], A[j] = A[j], A[i]
            i += 1
            j -= 1
        else:
            break
    if j > 0:
        quickSortR(A, 0, j)
    if N > i:
        quickSortR(A, i, N - i)

def main():
    A = [27, 17, 3, 16, 13, 10, 1, 5, 7, 12, 4, 8, 9, 0]

    quickSortR(A, 0, len(A))

    k = 1