if __name__ == '__main__':
    import isGreather
    raise SystemExit(isGreather.main())


def solution(A):
    min_value = 10001

    for idx in range(0, len(A) - 1):
        if (abs(A[idx] + A[idx + 1])) < min_value:
            min_value = abs(A[idx] + A[idx + 1])
        if idx < len(A) - 2 and (abs(A[idx] + A[idx + 1] + A[idx + 2])) < min_value:
            min_value = abs(A[idx] + A[idx + 1] + A[idx + 2])

    return min_value

def main():
    A = [2, -4, 6, -3, 9]
    val = solution(A)

    mm = 11