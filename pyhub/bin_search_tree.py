if __name__ == '__main__':
    import bin_search_tree
    raise SystemExit(bin_search_tree.main())


class Node:
    def __init__(self, left, right, parent, value):
        self.left = left
        self.right = right
        self.parent = parent
        self.value = value

    def __init__(self):
        self.left = None
        self.right = None
        self.parent = None
        self.value = 0


class BSTree:
    def __init__(self):
        self.tree = None

    def insert(self, value):
        current = self.tree

        parent = None

        while current is not None:
            if value == current.value:
                return current

            parent = current

            if value < current.value:
                current = current.left
            else:
                current = current.right

        new_node = Node(None, None, parent, value)

        if parent is not None:
            if value < parent.value:
                parent.left = new_node
            else:
                parent.right = new_node
        else:
            self.tree = new_node

        return new_node

    def traverse(self, root):
        if root is not None:
            self.traverse(root.left)
            print(root.value)
            self.traverse(root.right)


    def traverse_no_recursion(self, root):
        stack = []
        current = root

        while True:
            while current is not None:
                stack.append(current)
                current = current.left
            if current is None and len(stack) > 0:
                item = stack.pop()
                print(item.value)
                current = item.right
            else:
                break


    def traverse_no_recursion2(self, root):
        current = root

        while current is not None:
            if current.left is None:
                print(current.value)
                current = current.right
            else:
                # TODO : append this
                pass

    def breadth_first_search(self, value):
        if self.tree is None or value == self.tree.value:
            return self.tree
        q = [self.tree]

        while len(q) > 0:
            node = q.pop()
            if node.value == value:
                return node
            else:
                if node.left is not None:
                    q.append(node.left)
                if node.right is not None:
                    q.append(node.right)
        return None

    def get_max_width(self):
        if self.tree is None:
            return 0
        retval = 1

        q = [self.tree]

        while len(q) > 0:
            tmp_q = q.copy();
            q.clear()

            for e in tmp_q:
                if e.left is not None:
                    q.append(e.left)
                if e.right is not None:
                    q.append(e.right)

            retval = max(retval, len(q))
        return retval

    def tree_search(self, tree, value):
        if tree is None or value == tree.value:
            return tree
        if value < tree.value:
            self.tree_search(tree.left, value)
        else:
            self.tree_search(tree.right, value)

    def iterative_search(self, value):
        current = self.tree
        while current is not None and value != current.value:
            if value < current.value:
                current = current.left
            else:
                current = current.right
        return current

    def minimum(self, tree):
        while tree.left is not None:
            tree = tree.left
        return tree

    def maximum(self, tree):
        while tree.right is not None:
            tree = tree.right
        return tree

    def successor(self, tree):
        if tree.right is not None:
            return self.minimum(tree.right)
        parent = tree.parent
        while parent is not None and tree == parent.right:
            tree = parent
            parent = parent.parent
        return parent

    def predecessor(self, tree):
        if tree.left is not None:
            return self.maximum(tree)
        parent = tree.parent
        while parent is not None and tree == parent.left:
            tree = parent
            parent = parent.parent
        return parent


    def _transplant(self, node, new_node):

        if node.parent is None:
            self.tree = new_node
        elif node.parent.left == node:
            node.parent.left = new_node
        else:
            node.parent.right = new_node

        if new_node is not None:
            new_node.parent = node.parent

    def erase(self, node):
        if node.left is None or node.right is None:
            if node.left is None:
                self._transplant(node, node.right)
            else:
                self._transplant(node, node.left)
        else:
            # We want to splice y out of its current location and have it replace Node in the tree.
            y = self.successor(node)

            if y.parent != node:
                self._transplant(y, y.right)
                y.right = node.right
                y.right.parent = y
            self._transplant(node, y)
            y.left = node.left
            y.left.parent = y

    def depth(self, tree):
        if tree is None:
            return 0
        return 1 + max(self.depth(tree.left), self.depth(tree.right))

    @staticmethod
    def height(tree):
        retval = 0
        while tree is not None:
            retval += 1
            tree = tree.parent

        return retval

    @staticmethod
    def LCA(node1, node2):
        h1 = BSTree.height(node1)
        h2 = BSTree.height(node2)

        while h1 != h2:
            if h1 > h2:
                node1 = node1.parent
                h1 = h1 - 1
            else:
                node2 = node2.parent
                h2 = h2 - 1

        while node1 != node2:
            node1 = node1.parent
            node2 = node2.parent

        return node1

    def to_dll(self):
        current = self.tree
        stack = []

        head = None
        current_dll_node = None

        while True:
            while current is not None:
                stack.append(current)
                current = current.left
            if current is None and len(stack) > 0:
                item = stack.pop()
                #PUSH TO DLL

                if head is None:
                    head = Node
                    head.value = item.value
                    current_dll_node = head
                else:
                    new_node = Node
                    new_node.value = item.value
                    current_dll_node.right = new_node
                    new_node.left = current_dll_node
                    current_dll_node = new_node

                current = item.right
            else:
                break
        return  head

def main():
    tree = BSTree()

    tree.insert(24)
    tree.insert(27)
    tree.insert(22)
    tree.insert(15)
    tree.insert(20)
    tree.insert(23)
    tree.insert(4)
    tree.insert(26)
    tree.insert(17)
    tree.insert(18)
    tree.insert(13)
    tree.insert(5)
    tree.insert(3)
    node1 = tree.insert(6)
    node2 = tree.insert(21)



    val = tree.iterative_search(21)
    succ = tree.predecessor(val)

    LCANode = BSTree.LCA(node1, node2)

    tree.erase(val)

    tree.traverse(tree.tree)