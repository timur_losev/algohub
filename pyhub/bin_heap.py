if __name__ == '__main__':
    import bin_heap
    raise SystemExit(bin_heap.main())


def parent(i):
    return i >> 1


def left(i):
    return (i << 1) + 1


def right(i):
    return left(i) + 1


def min_heapify(i, heap, heap_size):
    l = left(i)
    r = right(i)
    lowest = 0
    if l < heap_size and heap[l] < heap[i]:
        lowest = l
    else:
        lowest = i

    if r < heap_size and heap[r] < heap[lowest]:
        lowest = r

    if lowest != i:
        heap[i], heap[lowest] = heap[lowest], heap[i]
        min_heapify(lowest, heap, heap_size)


def max_heapify(i, heap, heap_size):
    l = left(i)
    r = right(i)
    largest = 0
    if l < heap_size and heap[l] > heap[i]:
        largest = l
    else:
        largest = i

    if r < heap_size and heap[r] > heap[largest]:
        largest = r

    if largest != i:
        p = heap[i]
        heap[i] = heap[largest]
        heap[largest] = p
        max_heapify(largest, heap, heap_size)


def build_max_heap(heap, heap_size):
    i = (heap_size >> 1) - 1
    while i >= 0:
        max_heapify(i, heap, heap_size)
        i -= 1

def main():
    heap = [27, 17, 3, 16, 13, 10, 1, 5, 7, 12, 4, 8, 9, 0]

    heap_size = len(heap)

    i = heap_size - 1
    while i >= 1:
        heap[0], heap[i] = heap[i], heap[0]
        heap_size -= 1
        min_heapify(0, heap, heap_size)
        i -= 1

    build_max_heap(heap, heap_size)

    i = heap_size - 1
    while i >= 1:
        heap[0], heap[i] = heap[i], heap[0]
        heap_size -= 1
        max_heapify(0, heap, heap_size)
        i -= 1

class Heap:

    heap = []
    heap_size = 0

    def __init__(self, in_heap):
        self.heap = in_heap
        self.heap_size = len(self.heap)

    @staticmethod
    def parent(i):
        return i >> 1

    @staticmethod
    def left(i):
        return (i << 1) + 1

    @staticmethod
    def right(i):
        return Heap.left(i) + 1

    def min_heapify(self, i):
        l = Heap.left(i)
        r = Heap.right(i)

        if l < self.heap_size and self.heap[l] < self.heap[i]:
            smallest = l
        else:
            smallest = i

        if r < self.heap_size and self.heap[r] < self.heap[smallest]:
            smallest = r

        if smallest != i:
            self.heap[i], self.heap[smallest] = self.heap[smallest], self.heap[i]
            self.min_heapify(smallest)

    def max_heapify(self, i):
        l = Heap.left(i)
        r = Heap.right(i)

        if l < self.heap_size and self.heap[l] > self.heap[i]:
            largest = l
        else:
            largest = i

        if r < self.heap_size and self.heap[r] > self.heap[largest]:
            largest = r

        if largest != i:
            self.heap[i], self.heap[largest] = self.heap[largest], self.heap[i]
            self.max_heapify(largest)

    def build_min_heap(self):
        i = (self.heap_size >> 1) - 1
        while i >= 0:
            self.min_heapify(i)
            i -= 1

    def build_max_heap(self):
        i = (self.heap_size >> 1) - 1
        while i >= 0:
            self.max_heapify(i)
            i -= 1

    def extract_max(self):
        if self.heap_size == 0:
            return 0

        max = self.heap[0]
        self.heap[0] = self.heap[self.heap_size - 1]
        self.heap_size -= 1
        self.heap.pop()
        self.max_heapify(0)

        return max

    def extract_min(self):
        if self.heap_size == 0:
            return 0

        min = self.heap[0]
        self.heap[0] = self.heap[self.heap_size - 1]
        self.heap_size -= 1
        self.heap.pop()
        self.min_heapify(0)

        return min

    def sort_min(self):
        i = self.heap_size - 1
        while i >= 1:
            self.heap[0], self.heap[i] = self.heap[i], self.heap[0]
            self.heap_size -= 1
            self.min_heapify(0)
            i -= 1

    def sort(self):
        i = self.heap_size - 1
        while i >= 1:
            self.heap[0], self.heap[i] = self.heap[i], self.heap[0]
            self.heap_size -= 1
            self.max_heapify(0)
            i -= 1

    def insert_key_max(self, key):
        i = self.heap_size
        self.heap_size += 1
        self.heap.push(key)
        while i > 0 and self.heap[Heap.parent(i)] < self.heap[i]:
            self.heap[i], self.heap[Heap.parent(i)] = self.heap[Heap.parent(i)], self.heap[i]
            i = Heap.parent(i)

#Question: Write an efficient program for printing k largest elements in an array. Elements in array can be in any order.
def find_k_largest_elements(arr, k):
    heap = Heap(arr)

    retval = []
    for i in range(0, k):
        retval.append(heap.extract_max())
    return retval

def main():
    heap_array = [27, 17, 3, 16, 13, 10, 1, 5, 7, 12, 4, 8, 9, 0]

    k_max = find_k_largest_elements(heap_array, 4)

    vaa = 0
