if __name__ == '__main__':
    import ArrayTasks
    raise SystemExit(ArrayTasks.main())

import sys

def find_range_for_sorting(arr):
    n = -1
    m = -1
    for i in range(0, len(arr)):
        m = i
        for j in range(i + 1, len(arr)):
            if arr[i] > arr[j] or (arr[i] >= arr[j] and (j-i) > 1):
                n = j
    print(m,n, sep=",")


def find_the_minimum_and_maximum_values_that_can_be_calculated(arr):
    excluded = 0
    sum = 0
    min_sum = 9999999
    for i in range(0, len(arr)):
        local_sum = 0
        for j, item in enumerate(arr):
            if j == excluded:
                continue
            local_sum += item

        sum = max(sum, local_sum)
        min_sum = min(sum, local_sum)
        excluded += 1

    print(min_sum, sum, sep=' ')


def find_count_of_maxes(arr):
    count = 1

    arr.sort(reverse=True)
    current_max = arr[0]

    for i in range(1, len(arr)):
        if arr[i] == current_max:
            count += 1
        else:
            break
    return count


def find_the_minimum_and_maximum_values_that_can_be_calculated2(arr):
    whole_sum = sum(arr)

    max_sum = 0
    min_sum = sys.maxsize

    for i in arr:
        val = whole_sum - i
        max_sum = max(max_sum, val)
        min_sum = min(min_sum, val)

    print(min_sum, max_sum, sep=' ')

def find_the_average_within_a_frame(arr, frame):
    # find sum in a frame
    # sum / frame

    # b-force:
    max = len(arr) - frame
    if max <= 0:
        pass #return sum or whole array/len

    retval1 = []
    i = 0
    j = frame
    while j < len(arr):
        sum = 0
        for k in range(i, j):
            sum += arr[k]
        retval1.append(sum / frame)
        i += 1
        j += 1

############MOVING_SUM#############

    sum = 0
    for i in range(0, frame):
        sum += arr[i]

    i = 1
    j = frame
    retval2 = []
    retval2.append(sum / frame)

    #sum = (sum - arr[i-1] + arr[j]) / frame
    while j < len(arr):
        sum = (sum - arr[i-1] + arr[j])
        retval2.append(sum / frame)
        j += 1
        i += 1

    return retval2

def find_max_sum(arr):
    prefix_sum = []
    for i in range(len(arr)):
        run_sum = arr[i];
        for j in range(i + 1, len(arr)):
            run_sum += arr[j]
        prefix_sum.append(run_sum)

    retval = -999999
    for i in range(len(prefix_sum)):
        sum = prefix_sum[i]
        for j in range(i + 1, len(prefix_sum)):
            sum -= arr[j]
        retval = max(sum, retval)

    return retval

def bubble_sort(arr):
    for i in range(len(arr)):
        for j in range(i + 1, len(arr)):
            if arr[i] > arr[j]:
                arr[i], arr[j] = arr[j], arr[i]

def quick_sort(arr):
    pass

def main():

    myArr = [1,2,3,4,5]

    v = myArr[:2];

    #arr = [1, 2, 4, 7, 10, 11, 7, 12, 6, 7, 16, 18, 19]

    # ans 3, -2, 4 = 5
    arr2 = [2  , -8 , 3 , -2, 4 , -10]
  # sums = [-11, -13, -5, -8, -6, -10]

    find_max_sum(arr2)

    average = find_the_average_within_a_frame(arr, 3)


    find_range_for_sorting(arr)
    arr = [10, 9, 8, 7]
    bubble_sort(arr)

    '''find_range_for_sorting(arr)

    arr_for_sum = [140638725, 436257910, 953274816, 734065819, 362748590]
    find_the_minimum_and_maximum_values_that_can_be_calculated2(arr_for_sum)'''



