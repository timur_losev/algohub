from enum import Enum

if __name__ == '__main__':
    import rbtree

    raise SystemExit(rbtree.main())


class Color(Enum):
    Red = 0
    Black = 1


class RBNode:
    def __init__(self, left, right, parent, color, value):
        self.left = left
        self.right = right
        self.parent = parent
        self.color = color
        self.value = value


leaf = RBNode(None, None, None, Color.Black, 0)
leaf.left = leaf
leaf.right = leaf

NIL = leaf


class RBTree:
    def __init__(self):
        self.tree = NIL

    def rotate_left(self, x):
        y = x.right  # set y
        x.right = y.left  # turn y's left subtree into x's right subtree

        if y.left != NIL:
            y.left.parent = x

        y.parent = x.parent  # link x's parent to y
        if x.parent == NIL:
            self.tree = y
        elif x == x.parent.left:
            x.parent.left = y
        else:
            x.parent.right = y

        y.left = x  # put x on y's left
        x.parent = y

    def rotate_right(self, y):
        x = y.left
        y.left = x.right

        if x.right == NIL:
            x.right.parent = y

        x.parent = y.parent

        if y.parent == NIL:
            self.tree = x
        elif y == y.parent.left:
            y.parent.left = x
        else:
            y.parent.right = x

    def insert_fixup(self, z):
        while z.color == Color.Red:
            if z.parent == z.parent.parent.left:
                pass

    def insert(self, value):
        current = self.tree
        parent = None

        while current != NIL:
            if value == current.value:
                return current

            parent = current

            if value < current.value:
                current = current.left
            else:
                current = current.right

        new_node = RBNode(NIL, NIL, parent, Color.Red, value)

        if parent is not None:
            if value < parent.value:
                parent.left = new_node
            else:
                parent.right = new_node
        else:
            self.tree = new_node


def main():
    tree = RBTree()

    tree.tree = RBNode(NIL, NIL, NIL, Color.Black, 7)
    tree.tree.left = RBNode(NIL, NIL, tree.tree, Color.Red, 4)
    tree.tree.left.right = RBNode(NIL, NIL, tree.tree.left, Color.Red, 6)
    tree.tree.left.left = RBNode(NIL, NIL, tree.tree.left, Color.Red, 3)
    tree.tree.left.left.left = RBNode(NIL, NIL, tree.tree.left.left, Color.Red, 2)

    tree.tree.right = RBNode(NIL, NIL, tree.tree, Color.Red, 11)
    tree.tree.right.left = RBNode(NIL, NIL, tree.tree.right, Color.Red, 9)
    tree.tree.right.right = RBNode(NIL, NIL, tree.tree.right, Color.Red, 18)
    tree.tree.right.right.left = RBNode(NIL, NIL, tree.tree.right.right, Color.Red, 14)
    tree.tree.right.right.left.left = RBNode(NIL, NIL, tree.tree.right.right.left, Color.Red, 12)
    tree.tree.right.right.left.right = RBNode(NIL, NIL, tree.tree.right.right.left, Color.Red, 17)

    tree.tree.right.right.right = RBNode(NIL, NIL, tree.tree.right.right, Color.Red, 19)
    tree.tree.right.right.right.right = RBNode(NIL, NIL, tree.tree.right.right.right, Color.Red, 22)
    tree.tree.right.right.right.right.left = RBNode(NIL, NIL, tree.tree.right.right.right.right, Color.Red, 20)

    tree.rotate_left(tree.tree.right)
